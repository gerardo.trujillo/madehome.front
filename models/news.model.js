const { Schema, model } = require('mongoose');

const NewsSchema =  Schema({
    title: {
        type: JSON,
        required: [true, 'El nombre es obligatorio']
    },
    slug: {
        type: String,
        unique: true,
        required: [true, 'El slug es obligatorio']
    },
    intro: {
        type: JSON,
    },
    url: {
        type: String,
    },
    content: {
        type: JSON,
    },
    image: {
        type: String,
    },
    date: {
        type: Date
    },
    images: [{
        type: String
    }],
    active: {
        type: Boolean,
        default: true
    },
    delete: {
        type: Boolean,
        default: false
    }
});

NewsSchema.methods.toJSON = function () {
    const { __v, ...model } = this.toObject();
    return model;
}

module.exports = model('News',  NewsSchema);

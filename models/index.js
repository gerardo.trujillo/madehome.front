const Category = require('./category.model');
const News = require('./news.model');
const ResetPassword = require('./resetPassword.model');
const User = require('./user.model');
const Server = require('./server');

module.exports = {
    Category,
    News,
    ResetPassword,
    User,
    Server
}
